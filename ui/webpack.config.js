const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpackMerge = require('webpack-merge');
const modeConfig = env => require(`./buildUtils/webpack.${env}`)(env);
const presetsConfig = require('./buildUtils/loadPresets');

const source = path.resolve('./src');

module.exports = ({ mode, presets } = { mode: 'production', presets: [] }) =>
	webpackMerge(
		{
			entry: [path.join(source, 'index.tsx')],
			mode,
			output: {
				path: path.join(__dirname, 'dist'),
				filename: 'bundle.js'
			},
			module: {
				rules: [
					{
						test: /\.(ts|tsx)?$/,
						loader: 'awesome-typescript-loader'
					},
					{
						enforce: 'pre',
						test: /\.js$/,
						loader: 'source-map-loader'
					},
					{
						test: /\.(jpg|png|svg)$/,
						use: [
							{ loader: 'url-loader', options: { limit: 5000 } }
						]
					}
				]
			},
			plugins: [
				new HtmlWebpackPlugin({
					template: path.join(source, 'index.html')
				}),
				new webpack.ProgressPlugin()
			],
			resolve: {
				extensions: ['.scss', '.ts', '.tsx', '.js', '.jsx']
			}
		},
		modeConfig(mode),
		presetsConfig({ mode, presets })
	);
