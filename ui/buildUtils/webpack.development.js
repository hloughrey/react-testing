const path = require('path');

module.exports = () => ({
	module: {
		rules: [
			{
				test: /\.scss?$/,
				use: [
					'style-loader',
					{
						loader: 'css-loader',
						options: {
							camelCase: true,
							importLoaders: true,
							localIdentName: '[name]_[local]_[hash:base64:5]',
							minimize: true,
							modules: true,
							namedExport: true
						}
					},
					'sass-loader'
				]
			}
		]
	}
});
