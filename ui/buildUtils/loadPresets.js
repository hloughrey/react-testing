const webpackMerge = require('webpack-merge');

const applyPresets = env => {
	const { presets } = env;

	if (!presets) return {};

	const mergePresets = [].concat(...[presets]);
	const mergeConfigs = mergePresets.map(presetName =>
		require(`./presets/webpack.${presetName}`)(env)
	);

	return webpackMerge({}, ...mergeConfigs);
};

module.exports = applyPresets;
