module.exports = {
	moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
	moduleNameMapper: {
		'^.+\\.(css|scss)$': 'identity-obj-proxy'
	},
	transform: {
		'\\.(js|jsx|ts|tsx)$': 'babel-jest'
	},
	testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(ts|tsx|js|jsx)?$',
	collectCoverageFrom: [
		'**/src/components/**/*.spec.jsx',
		'**/src/components/**/*-logic.spec.js'
	],
	coverageThreshold: {
		global: {
			statements: 50,
			branches: 50,
			functions: 50,
			lines: 50
		}
	}
};
