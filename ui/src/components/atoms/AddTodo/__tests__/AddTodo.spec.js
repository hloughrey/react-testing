import React from 'react';
import AddTodo from '../';
import { render, fireEvent, cleanup } from 'react-testing-library';

afterEach(cleanup);

describe('Add Todo component', () => {
	it('calls the given callback prop with the new text', async () => {
		const todoCallback = jest.fn();

		const { getByLabelText, getByTestId } = render(
			<AddTodo onNewTodo={todoCallback} />
		);
		fireEvent.change(getByLabelText('Add an item'), {
			target: { value: 'Write a letter to Santa' }
		});
		fireEvent.click(getByTestId('submit'));

		expect(todoCallback).toHaveBeenCalledTimes(1);
		expect(todoCallback).toHaveBeenCalledWith({
			name: 'Write a letter to Santa'
		});
	});
});
