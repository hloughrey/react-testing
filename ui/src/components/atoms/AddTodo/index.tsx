import * as React from 'react';
import { IState, IProps } from './types';

const s = require('./styles.scss');

class AddTodo extends React.Component<IProps, IState> {
	state: IState = {
		todo: ''
	};

	handleToDoInput = (e: any) => {
		this.setState({ todo: e.target.value });
	};

	addTodo = (e: any) => {
		e.preventDefault();

		if (this.state.todo) {
			this.props.addTodo(this.state.todo);
		}
		this.setState({ todo: '' });
	};

	render() {
		return (
			<form onSubmit={this.addTodo} className={s.addTodo}>
				<label className={s.addTodoLabel} htmlFor="toDoInput">
					Add an item
				</label>
				<input
					id="toDoInput"
					type="text"
					placeholder="Walk the dog"
					value={this.state.todo}
					onChange={this.handleToDoInput}
				/>
				<button data-testid="submit" type="submit">
					Add Todo
				</button>
			</form>
		);
	}
}

export default AddTodo;
