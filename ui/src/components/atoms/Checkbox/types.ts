export interface IState {
	todo: string;
}

export interface IProps {
	addTodo: (todo: string) => void;
}
