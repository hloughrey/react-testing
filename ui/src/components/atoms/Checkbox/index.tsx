import * as React from 'react';
const s = require('./styles.scss');

const Checkbox = () => (
	<input
		className={s.checkbox}
		type="checkbox"
		name="vehicle1"
		value="Bike"
	/>
);

export default Checkbox;
