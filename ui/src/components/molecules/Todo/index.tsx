import * as React from 'react';
import { IProps } from './types';
import Checkbox from '../../atoms/Checkbox';

const s = require('./styles.scss');

class Todo extends React.Component<IProps, {}> {
	toggleDone = (e: any) =>
		this.props.doneChange(e.target.getAttribute('data-value'));

	deleteTodo = (e: any) => {
		e.preventDefault();
		this.props.deleteTodo(e.target.getAttribute('data-value'));
	};

	render() {
		const { todo } = this.props;

		return (
			<li className={s.todo}>
				<Checkbox />
				{todo.name}
				<a
					data-testid="delete-todo-item"
					className={s.deleteTodo}
					href="#"
					onClick={this.deleteTodo}
					data-value={todo.id}
				>
					Delete
				</a>
			</li>
		);
	}
}

export default Todo;
