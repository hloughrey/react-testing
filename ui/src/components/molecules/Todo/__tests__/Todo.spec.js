import React from 'react';
import Todo from '../';
import { render, fireEvent, cleanup } from 'react-testing-library';

afterEach(cleanup);

describe('Todo component', () => {
	it('renders todo item name', () => {
		const todo = { id: 1, name: 'Buy Milk', done: false };
		const { getByTestId } = render(<Todo todo={todo} />);
		const item = getByTestId('todo-item');
		expect(item.textContent).toEqual('Buy Milk');
	});

	it('renders a not-done todo item', () => {
		const todo = { id: 1, name: 'Buy Milk', done: false };
		const { getByTestId } = render(<Todo todo={todo} />);
		const item = getByTestId('todo-item');
		expect(item.textContent).toEqual('Buy Milk');
		expect(item.className).toBe('');
	});

	it('renders a done todo item', () => {
		const todo = { id: 1, name: 'Buy Milk', done: true };
		const { getByTestId } = render(<Todo todo={todo} />);
		const item = getByTestId('todo-item');
		expect(item.className).toBe('doneTodo');
	});

	it('toggling a TODO calls the given prop', () => {
		const doneCallback = jest.fn();
		const todo = { id: 1, name: 'Buy Milk', done: false };
		const { getByTestId } = render(
			<Todo todo={todo} doneChange={doneCallback} />
		);
		const item = getByTestId('todo-item');
		fireEvent.click(item);
		expect(doneCallback).toHaveBeenCalledTimes(1);
		expect(doneCallback).toHaveBeenCalledWith(1);
	});

	it('deleting a TODO calls the given prop', () => {
		const deleteCallback = jest.fn();
		const todo = { id: 1, name: 'Buy Milk', done: false };
		const { getByTestId } = render(
			<Todo todo={todo} deleteTodo={deleteCallback} />
		);
		const item = getByTestId('delete-todo-item');

		fireEvent.click(item);

		expect(deleteCallback).toHaveBeenCalledTimes(1);
	});
});
