import { ITodo } from '../../../types';

export interface IProps {
	todo: ITodo;
	doneChange: (id: number) => void;
	deleteTodo: (id: number) => void;
}
