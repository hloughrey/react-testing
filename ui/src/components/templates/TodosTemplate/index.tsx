import * as React from 'react';
// import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';
import Todo from '../../molecules/Todo';
import AddTodo from '../../atoms/AddTodo';
import { getTodos, addTodo, deleteTodo} from '../../../redux/actions';
import { ITodo } from '../../../types';
import { getTodosFromState } from '../../../redux/selectors';

const s = require('./styles.scss');

interface IProps {
	getTodos: () => void;
	addTodo: (todo: string) => void;
	deleteTodo: (id: number) => void
	todos: ITodo[];
}

class TodosTemplate extends React.Component<IProps, {}> {
	componentDidMount() {
		this.props.getTodos();
	}

	renderTodos = () =>
		this.props.todos.map(todo => (
			<Todo
				todo={todo}
				doneChange={() => console.log('foo')}
				deleteTodo={this.props.deleteTodo}
			/>
		));

	render() {
		return this.props.todos ? (
			<div className={s.mainContainer}>
				<h1 className={s.todoTitle}>To-Do List</h1>
				<div className={s.todoContainer}>
					<AddTodo addTodo={this.props.addTodo} />
					<ul data-testid="todos-list" className={s.todoList}>
						{this.renderTodos()}
					</ul>
				</div>
			</div>
		) : (
			<h2>Loading</h2>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	getTodos: () => dispatch(getTodos()),
	addTodo: (todo: string) => dispatch(addTodo(todo)),
	deleteTodo: (id: number) => dispatch(deleteTodo(id))
});

const mapStateToProps = state => ({
	todos: getTodosFromState(state)
});

// export default hot(module)(TodosTemplate);
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TodosTemplate);
