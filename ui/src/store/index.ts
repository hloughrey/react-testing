import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import sagas from '../redux/sagas';
import rootReducer from '../redux/reducers';

const sagaMiddleware = createSagaMiddleware()

const composeEnhancers =
    (typeof window !== 'undefined' &&
        (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
        (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            serialize: true
        })) ||
    compose;

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(sagaMiddleware)));

Object.keys(sagas).forEach(sagaKey => sagaMiddleware.run(sagas[sagaKey]));

export default store;
