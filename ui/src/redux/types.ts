import { ITodosState } from './todos/types';

export default interface IStoreState {
    todos: ITodosState;
}
