import types from './types';

const todos = (state = [], action) => {
	switch (action.type) {
		case types.GET_TODOS:
			return [...state];
		case types.GET_TODOS_SUCCESS:
			return [...action.payload];
		case types.ADD_TODO:
			return [...state];
		case types.DELETE_TODO:
			return [...state];
		case types.ADD_TODO_SUCCESS:
			return [...state, action.payload.data];
		case types.TOGGLE_TODO:
			return state.map(
				todo =>
					todo.id === action.id
						? { ...todo, completed: !todo.completed }
						: todo
			);
		default:
			return state;
	}
};

export default todos;
