import { takeLatest, call, put } from 'redux-saga/effects';
import types from './types';
import {
	getTodos as getTodosAction,
	getTodosSuccess,
	addTodoSuccess
} from './actions';
import {
	getTodos as getTodosAPI,
	addTodo as addTodoAPI,
	deleteTodo as deleteTodoAPI
} from '../../services/todos';

export function* getTodos() {
	try {
		const todos = yield call(getTodosAPI);
		yield put(getTodosSuccess(todos));
	} catch (e) {
		console.log('Error');
	}
}

export function* addTodo(action) {
	try {
		const todo = yield call(addTodoAPI, action.payload);
		yield put(addTodoSuccess(todo.data));
	} catch (e) {
		console.log(e);
	}
}

export function* deleteTodo(action) {
	try {
		yield call(deleteTodoAPI, action.payload);
		yield put(getTodosAction());
	} catch (e) {
		console.log(e);
	}
}

export default {
	*getTodosSaga() {
		yield takeLatest(types.GET_TODOS, getTodos);
	},
	*addTodoSaga() {
		yield takeLatest(types.ADD_TODO, addTodo);
	},
	*deleteTodoSaga() {
		yield takeLatest(types.DELETE_TODO, deleteTodo);
	}
};
