import types from './types';
import { ITodo } from '../../types';
import { tsConstructorType } from '@babel/types';

export const getTodos = () => ({
	type: types.GET_TODOS
});

export const getTodosSuccess = res => {
	return {
		type: types.GET_TODOS_SUCCESS,
		payload: res.data
	};
};

export const addTodo = (todo: string) => ({
	type: types.ADD_TODO,
	payload: todo
});

export const addTodoSuccess = (data: ITodo) => ({
	type: types.ADD_TODO_SUCCESS,
	payload: { data }
});

export const deleteTodo = (id: number) => ({
	type: types.DELETE_TODO,
	payload: id
});

export const setVisibilityFilter = filter => ({
	type: types.SET_VISIBILITY_FILTER,
	filter
});

export const toggleTodo = id => ({
	type: types.TOGGLE_TODO,
	id
});

export const VisibilityFilters = {
	SHOW_ALL: 'SHOW_ALL',
	SHOW_COMPLETED: 'SHOW_COMPLETED',
	SHOW_ACTIVE: 'SHOW_ACTIVE'
};
