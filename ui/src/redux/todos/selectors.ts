import State from '../types';
import { ITodosState } from './types';

const selector = <T>(fn: (todos: ITodosState) => T) => (state: State) =>
	fn(state.todos);

export const getTodosFromState = selector(state => state);
