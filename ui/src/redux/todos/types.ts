import { ITodo } from '../../types';

export interface ITodosState {
	todos: ITodo[];
}

export default {
	GET_TODOS: Symbol('GET_TODOS'),
	GET_TODOS_SUCCESS: Symbol('GET_TODOS_SUCCESS'),
	ADD_TODO: Symbol('ADD_TODO'),
	ADD_TODO_SUCCESS: Symbol('ADD_TODO_SUCCESS'),
	DELETE_TODO: Symbol('DELETE_TODO'),
	DELETE_TODO_SUCCESS: Symbol('DELETE_TODO_SUCCESS'),
	SET_VISIBILITY_FILTER: Symbol('SET_VISIBILITY_FILTER'),
	TOGGLE_TODO: Symbol('TOGGLE_TODO')
};
