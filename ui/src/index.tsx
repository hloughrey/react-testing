import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Store from './store';

import TodosTemplate from './components/templates/TodosTemplate';

const app = (
	<Provider store={Store}>
		<TodosTemplate />
	</Provider>
);

ReactDOM.render(app, document.getElementById('app'));
