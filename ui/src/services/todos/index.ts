export const getTodos = () =>
	fetch('http://localhost:3000/todos').then(res => {
		if (res.status === 200) {
			return res.json();
		} else {
			throw new Error();
		}
	});

export const addTodo = (todo: string) =>
	fetch('http://localhost:3000/todos', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({ name: todo })
	})
		.then(res => {
			if (res.status === 200) {
				return res.json();
			} else {
				throw new Error();
			}
		})
		.catch(e => console.log(e));

export const deleteTodo = (id: number) =>
	fetch(`http://localhost:3000/todos/${id}`, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json'
		}
	})
		.then(res => {
			if (res.status === 200) {
				return res.json();
			} else {
				throw new Error();
			}
		})
		.catch(e => console.log(e));
