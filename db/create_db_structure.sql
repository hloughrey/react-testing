DO
$$DECLARE
    v_rec varchar;
    v_schema varchar[] := array[ 'todos' ];
BEGIN
    FOREACH v_rec IN ARRAY v_schema
        LOOP
            IF NOT EXISTS ( SELECT 1 FROM information_schema.schemata WHERE schema_name = v_rec ) THEN
                RAISE INFO 'Creating schema %', v_rec;
                -- E.g. CREATE SCHEMA foo;
                EXECUTE 'CREATE SCHEMA ' || v_rec || ';';
            ELSE
                RAISE INFO 'Schema % already exists', v_rec;
            END IF;
        END LOOP;
END$$;

DO
$$DECLARE
    v_database varchar := 'testing';
    v_schema varchar := 'todos';
    v_table varchar := 'todos';
BEGIN
    IF NOT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_catalog = v_database AND table_schema = v_schema AND table_name = v_table ) THEN
        RAISE INFO 'Creating table %.%', v_schema, v_table;
        /* E.g. CREATE TABLE foo.bar (
            id serial primary key,
            name varchar(25)
        );*/
        EXECUTE 'CREATE TABLE ' || v_schema || '.' || v_table || ' (
            id SERIAL PRIMARY KEY,
            name VARCHAR(50) NOT NULL,
            completed BOOLEAN DEFAULT false
        );';
    ELSE
        RAISE INFO 'TABLE %.% already exists', v_schema, v_table;
    END IF;
END$$;