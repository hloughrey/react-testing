const routes = require("express").Router();
const todos = require("./todos");

routes.get("/", (req, res) => {
    res.status(200).json({
        message: "Connected to API"
    });
});

// Route any request for messages
routes.use("/todos", todos);

module.exports = routes;
