const todos = require("express").Router();
const todosQueries = require("../../queries/todos");

todos.get("/", todosQueries.getTodos);
todos.get("/:id", todosQueries.getTodo);
todos.post("/", todosQueries.postTodo);
todos.delete("/:id", todosQueries.deleteTodo);

module.exports = todos;
