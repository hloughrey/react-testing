const credentials = require('../../lib/credentials').pgres;
const responses = require('../../lib/responses.js');

const promise = require('bluebird');

let options = {
    promiseLib: promise
};

let pgp = require('pg-promise')(options);

let connectionString = `postgres://${credentials.username}:${
    credentials.password
}@${credentials.host}:${credentials.port}/${credentials.database}`;
let db = pgp(connectionString);

const getTodos = (req, res, next) => {
    db.any('SELECT * FROM todos.todos;')
        .then(data => {
            if (data) {
                res.status(200).json({
                    response: responses.RESPONSES_STATUS.SUCCESS,
                    message: responses.RESPONSES_MESSAGES.RESOURCE_FOUND,
                    data: data
                });
            } else {
                res.status(400).json({
                    response: responses.RESPONSES_STATUS.SUCCESS,
                    message: responses.RESPONSES_MESSAGES.RESOURCE_NOT_FOUND
                });
            }
        })
        .catch(err => {
            res.status(404).json({
                status: responses.RESPONSES_STATUS.ERROR,
                message: responses.RESPONSES_MESSAGES.RESOURCE_NOT_FOUND,
                data: {
                    name: err.name,
                    message: err.message
                }
            });
        });
};

const getTodo = (req, res, next) => {
    db.oneOrNone('SELECT * FROM todos.todos WHERE id = ${id};', {
        id: parseInt(req.params.id)
    })
        .then(data => {
            if (data) {
                res.status(200).json({
                    response: responses.RESPONSES_STATUS.SUCCESS,
                    message: responses.RESPONSES_MESSAGES.RESOURCE_FOUND,
                    data: data
                });
            } else {
                res.status(400).json({
                    response: responses.RESPONSES_STATUS.ERROR,
                    message: responses.RESPONSES_MESSAGES.RESOURCE_NOT_FOUND
                });
            }
        })
        .catch(err => {
            res.status(404).json({
                response: responses.RESPONSES_STATUS.ERROR,
                message: responses.RESPONSES_MESSAGES.RESOURCE_NOT_FOUND,
                data: {
                    name: err.name,
                    message: err.message
                }
            });
        });
};

const postTodo = (req, res, next) => {
    db.one('INSERT INTO todos.todos ("name") VALUES (${name}) RETURNING *;', {
        name: req.body.name
    })
        .then(data => {
            res.status(200).json({
                response: responses.RESPONSES_STATUS.SUCCESS,
                message: responses.RESPONSES_MESSAGES.RESOURCE_FOUND,
                data: data
            });
        })
        .catch(err => {
            res.status(404).json({
                response: responses.RESPONSES_STATUS.ERROR,
                message: responses.RESPONSES_MESSAGES.RESOURCE_NOT_FOUND,
                data: {
                    name: err.name,
                    message: err.message
                }
            });
        });
};

const deleteTodo = (req, res, next) => {
    db.none('DELETE FROM todos.todos WHERE id = ${id};', {
        id: parseInt(req.params.id)
    })
        .then(data => {
            console.log('data: ', data);
            res.status(200).json({
                response: responses.RESPONSES_STATUS.SUCCESS,
                message: responses.RESPONSES_MESSAGES.RESOURCE_FOUND
            });
        })
        .catch(err => {
            res.status(404).json({
                response: responses.RESPONSES_STATUS.ERROR,
                message: responses.RESPONSES_MESSAGES.RESOURCE_NOT_FOUND,
                data: {
                    name: err.name,
                    message: err.message
                }
            });
        });
};

module.exports = {
    getTodos,
    getTodo,
    postTodo,
    deleteTodo
};
